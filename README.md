![Logo](logo.png)

Monitoraggio delle proposte riguardanti i diritti digitali nei programmi elettorali delle elezioni 2022 in Italia.

Approfondimento: https://it.wikipedia.org/wiki/Elezioni_politiche_in_Italia_del_2022

## Temi
I temi principali monitorati sono i seguenti:
- Digital Divide
- Cybersecurity
- Privacy
- Promozione di software libero / sovranità digitale (italiana o almeno europea)
- Aumento videosorveglianza o introduzione di analisi biometrica/videosorveglianza con AI in tempo reale e altre tecnologie invasive

**Chiarimento:** mi è stato segnalato che la videosorveglianza è utile per la sicurezza. In questo monitoraggio è stata considerato negativo il suo aumento poiché è già presente in molte città in modo massiccio e non sono state trovate al momento fonti che confermano un aumento della rilevazione dei reati in proporzione al numero di installazioni di videocamere, anzi, una sproporzione tale rischia di far sentire le persone meno libere poiché sorvegliate continuamente. Possono eventualmente (parere personale) essere installate su strade cruciali per rilevare i veicoli in alcuni reati gravi.

## Metodo
Il punto di partenza è determinare quali possono essere i temi che secondo i cittadini garantiscono le libertà digitali.

I dati sono analizzati dai rispettivi programmi elettorali e sono valide anche dichiarazioni degli esponenti politici di ciascun partito durante la campagna elettorale.

Per l'analisi verrà dato un voto da **-1** a **+1** secondo questa scala:
- **-1**: proposta negativa che minaccia la privacy, la sicurezza o i diritti digitali delle persone
- **0**: argomento inserito nella proposta ma in modo neutrale o che non influisce sui diritti digitali
- **1**: proposta positiva che migliora i diritti digitali, alla privacy e alla sicurezza degli individui

Per evitare preferenze la lista sarà gestita in ordine alfabetico.

⚠️ **ATTENZIONE:** i programmi elettorali sono scaricabili dal sito del Ministero dell'Interno che sembra essere ospitato su server Amazon in USA, così come il server web di Possibile (poiché il programma non è presente nel sito del Ministero) è dietro firewall Cloudflare, noto MITM proxy che intercetta e analizza il traffico ad eccezione, per cui ho preferito scaricare una copia dei programmi e renderli scaricabili da questo stesso repository su gitea.it ospitato su server italiano e quindi sotto giurisdizione italiana e normativa europea sulla protezione dei dati personali (GDPR).

## Promesse elettorali
| Partito | Programma | Copia sicura | Temi trattati | Punteggio finale |
| :-- | :--: | :--: | :--: | :--: |
| **ALLEANZA VERDI E SINISTRA** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/65B/(65_progr_2_)-programma_di_alleanza_verdi_e_sinistra_in_pdfa.pdf) | [🔒 Scarica](programmi/(65_progr_2_)-programma_di_alleanza_verdi_e_sinistra_in_pdfa.pdf) | 7 | [+2](#alleanza-verdi-e-sinistra) |
| **AZIONE - ITALIA VIVA** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/7/(7_progr_2_)-programma_azione-italia_viva-calenda.pdf) | [🔒 Scarica](programmi/(7_progr_2_)-programma_azione-italia_viva-calenda.pdf) | 12 | [+4](#azione-italia-viva) |
| **FRATELLI D'ITALIA - FORZA ITALIA** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/68/(68_progr_2_)-programma.pdf) | [🔒 Scarica](programmi/(68_progr_2_)-programma.pdf) | 4 | [0](#fratelli-ditalia-forza-italia) |
| **ITALEXIT** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/16/(16_progr_2_)-italexit_per_l_italia.programma.pdf) | [🔒 Scarica](programmi/(16_progr_2_)-italexit_per_l_italia.programma.pdf) | 2 | [0](#italexit) |
| **ITALIA SOVRANA E POPOLARE** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/86/(86_progr_2_)-programma.pdf) | [🔒 Scarica](programmi/(86_progr_2_)-programma.pdf) | 0 | 0 |
| **IMPEGNO CIVICO** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/54/(54_progr_2_)-programma_impegno_civico-ldm-cd_autenticato.docx.pdf) | [🔒 Scarica](programmi/54/(54_progr_2_)-programma_impegno_civico-ldm-cd_autenticato.docx.pdf) | 0 | [0](#impegno-civico) |
| **LEGA PER SALVINI PREMIER** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/9/(9_progr_2_)-lega_per_salvini_premier.programma.pdf) | [🔒 Scarica](programmi/(9_progr_2_)-lega_per_salvini_premier.programma.pdf) | 15 | [+2](#lega-per-salvini-premier) |
| **MOVIMENTO 5 STELLE** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/74/(74_progr_2_)-programma_m5s_2022-cuore_e_coraggio.pdf) | [🔒 Scarica](programmi/(74_progr_2_)-programma_m5s_2022-cuore_e_coraggio.pdf) | 8 | [+3](#movimento-5-stelle) |
| **NOI MODERATI** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/48/(48_progr_2_)-programma.pdf) | [🔒 Scarica](programmi/(48_progr_2_)-programma.pdf) | 0 | 0 |
| **PARTITO DEMOCRATICO** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/75/(75_progr_2_)-programma_politiche_2022.pdf) | [🔒 Scarica](programmi/(75_progr_2_)-programma_politiche_2022.pdf) | 12 | [+4](#partito-democratico) |
| **POSSIBILE** | [⚠️ Originale](https://www.possibile.com/wp-content/uploads/2022/08/Possibile_Italia_Programma_2022.pdf) | [🔒 Scarica](programmi/Possibile_Italia_Programma_2022.pdf) | 8 | [+7](#possibile) |
| **+EUROPA** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/92/(92_progr_2_)-piu_europa.programma.pdf) | [🔒 Scarica](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/92/(92_progr_2_)-piu_europa.programma.pdf) | 3 | [+1](#+europa) |
| **UNIONE POPOLARE** | [⚠️ Originale](https://dait.interno.gov.it/documenti/trasparenza/POLITICHE_20220925/Documenti/83/(83_progr_2_)-programma_elettorale_a.pdf) | [🔒 Scarica](programmi/(83_progr_2_)-programma_elettorale_a.pdf) | 1 | [0](#unione-popolare) |


## Analisi dettagliata
Raccolta di punti del programma o dichiarazioni per il calcolo del punteggio:

### ALLEANZA VERDI E SINISTRA
> **3\. L’ITALIA DELLA MOBILITÀ SOSTENIBILE**
>>
> `digitalizzare` tutti i servizi di mobilità
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **3\. L’ITALIA DELLA MOBILITÀ SOSTENIBILE**
>
> È necessario favorire lo `smart working` per tutti i lavoratori e lavoratrici la cui presenza non è richiesta fisicamente.
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **8\. L’ITALIA DEL LAVORO**
>
> Il tempo per la cura di sé, degli affetti, degli ambienti, per lo sviluppo della propria cultura e dei propri talenti deve diventare l’orizzonte in cui ripensare tutto il lavoro anche attraverso l'`uso responsabile delle nuove tecnologie soprattutto nell'ambito del digitale`.
>
> **Punteggio:** +1
>
> **Motivo:** menzione all'uso responsabile di tecnologie digitali nel lavoro

> **8\. L’ITALIA DEL LAVORO**
>
> `L'innovazione tecnologica può diventare un'occasione per migliorare la nostra qualità della vita, oppure lo strumento per far aumentare i profitti di pochi e la disoccupazione di molti`.
>
> **Punteggio:** +1
>
> **Motivo:** riferimento chiaro all'innovazione tecnologia con pericolo di sfruttamento dalle Big Tech

> **9\. L’ITALIA GIUSTA**
>
> La tecnologia offre una grande occasione alla lotta contro l'evasione fiscale: è possibile, infatti, procedere alla `tracciabilità assoluta dei pagamenti`, anche promuovendo l'uso della moneta elettronica, e utilizzare le banche dati per incrociare i dati dei contribuenti, oltre che rafforzare la fatturazione elettronica e lo split payment, soprattutto sugli acquisti on line e tramite POS.
>
> **Punteggio:** -1
>
> **Motivo:** volontà di eliminazione del contante, tra i pochi metodi di pagamento non tracciabili e di libertà finanziaria

> **9\. L’ITALIA GIUSTA**
>
> Chiediamo che venga introdotto l'`obbligo di rendicontazione pubblica paese per paese, così da rendere trasparente dove e quanto facciano affari le corporation; che ci sia la massima trasparenza rispetto ai loro assetti proprietari`; che sia raddoppiata l’aliquota al 15% fissata dal G7; `che siano cancellati gli accordi segreti sul fisco stipulati dallo Stato italiano con le multinazionali estere`.
>
> **Punteggio:** +1
>
> **Motivo:** richiesta di maggiore trasparenza per le società


> **15\. L'ITALIA DELLA CULTURA**
>
> far fronte a cambiamenti, come quelli determinati dall'`era digitale`, che stravolgono completamente il rapporto di tutti e di tutte con la conoscenza e con le relazioni umane. Per questo diffondere una `pedagogia critica dell'era digitale`;
>
> **Punteggio:** 0
>
> **Motivo:** richiesta di maggiore attenzione ai cambiamenti digitali, ma non specificato con quali strumenti

---
### AZIONE - ITALIA VIVA
> **Produttività e crescita**
>>
> Ripristinare e rafforzare `Industria 4.0` aggiornando la lista dei beni agevolabili (includendo le nuove tecnologie) e aumentando il tetto massimo per gli investimenti
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **Energia e Ambiente e crescita**
>
> Oltre al sistema di irrigazione goccia a goccia, gli incentivi sarebbero accessibili anche a chi investe nel controllo delle fasi di irrigazione e di `micro-irrigazione digitale e da remoto`.
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **Giustizia**
>
> `Informatizzazione degli Uffici` e collegamento tra loro, delle Cancellerie di tutti gli Uffici giudiziari, con contestuale `creazione di archivi informatici per la raccolta di dati processuali`, con accesso gratuito per gli operatori, al fine di migliorare l'efficienza della macchina giudiziaria e l'`accessibilità da parte di cittadini` e operatori del settore.
>
> **Punteggio:** 0
>
> **Motivo:** miglioramento tecnologico, non chiari gli strumenti proposti

> **Diritti e pari opportunità**
>
> Rendere sistemici gli istituti sperimentati durante il COVID-19 a tutela dei lavoratori fragili: in particolare, nel caso di persone con disabilità o in condizioni di fragilità, il diritto al lavoro agile (c.d. `smart working`) da eccezione, deve divenire strumento strutturale.
>
> **Punteggio:** 0
>
> **Motivo:** miglioramento tecnologico, non chiari gli strumenti proposti

> **Welfare e Terzo Settore**
>
> Inoltre, `per garantire il più ampio accesso ai servizi pubblici in forma digitale`, è indispensabile accelerare i progetti di `contrasto al digital divide` (es: corsi di supporto alla digitalizzazione presso i centri per anziani o nei centri di aggregazione multifunzionali sul territorio)
>
> **Punteggio:** +1
>
> **Motivo:** contrasto al digital divide


> **Digitalizzare i processi partecipativi**
>
> Riteniamo necessario efficientare i principali processi partecipativi (es: raccolta firme per referendum) per consentire e facilitare una maggiore applicazione delle regole democratiche. A titolo esemplificativo è fondamentale digitalizzare i processi di `raccolta firme per referendum` e presentazione di liste elettorali nonché i `processi di consultazione pubblica`.
>
> **Punteggio:** +1
>
> **Motivo:** accesso tecnologico trasparente per la vita pubblica

> **Efficientamento dei processi della pubblica amministrazione**
>
> Per l'attuazione dei programmi dell'Agenda Digitale 2026, occorre un potenziamento delle struttura e delle figure chiave previste dal codice dell'amministrazione digitale, a partire dal `responsabile della transizione al digitale` e del difensore civico.
>
> **Punteggio:** 0
>
> **Motivo:** figura dedicata alla transizione digitale, non chiari gli strumenti proposti

> **Innovazione, digitale e space economy**
>
> Continuare a investire nella copertura delle reti ad altissima capacità (compresa la `fibra fino all'utente`) e alla `copertura 5G`.
>
> **Punteggio:** 0
>
> **Motivo:** tema tecnologico generico

> **Innovazione, digitale e space economy**
>
> Per questo, le Forze Armate devono incrementare gli investimenti nella formazione continua dei corpi specializzati nella `cybersecurity`, in pieno raccordo con le iniziative europee, con percorsi di certificazione delle competenze che abbiano valenza anche in ambito civile.
>
> **Punteggio:** +1
>
> **Motivo:** sensibilità al tema di cybersecurity


> **Innovazione, digitale e space economy**
>
> Sulla sfida dell'intelligenza artificiale, prendendo spunti da altri Paesi europei e per dare `piena implementazione alla strategia italiana di IA`, è necessaria un'unità dedicata del Ministero per l'Innovazione tecnologica oltre al coordinamento interministeriale previsto dalla strategia.
>
> **Punteggio:** 0
>
> **Motivo:** tema tecnologico ma non chiari gli strumenti e le garanzie

> **Innovazione, digitale e space economy**
>
> Si intende supportare la `transizione digitale` e 4.0 delle imprese, rafforzando il framework normativo originale di industria 4.0, potenziando e razionalizzando i soggetti a cui questo supporto alle imprese compete (Centri di Competenza e `Digital Innovation Hub`), sostenendo in particolare la crescita delle PMI, ma anche la neo-creata `Agenzia per la Cybersicurezza Nazionale` che deve essere anche partner delle aziende, non solo controllarle.
>
> **Punteggio:** 0
>
> **Motivo:** tema tecnologico ma non chiari gli strumenti e le garanzie

> **Innovazione, digitale e space economy**
>
> `I pericoli concreti non vengono scongiurati da una sorveglianza di massa`, ma da misure mirate, come l’osservazione ravvicinata di coloro che sono a rischio, come nel caso di soggetti radicalizzati che possono alimentare il circuito terroristico.
>
> **Punteggio:** +1
>
> **Motivo:** contro la sorveglianza di massa
---

### FRATELLI D'ITALIA - FORZA ITALIA
> **2\. Infrastrutture strategiche e utilizzo efficiente delle risorse europee**
>
> Potenziamento e sviluppo delle `infrastrutture digitali` ed estensione della `banda ultralarga` in tutta Italia
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **6\. Sicurezza e contrasto all'immigrazione illegale**
>
> Implementazione della sicurezza nelle città: rafforzamento operazione strade sicure, poliziotto di quartiere e `videosorveglianza`
>
> **Punteggio:** -1
>
> **Motivo:** aumento videosorveglianza

> **6\. Sicurezza e contrasto all'immigrazione illegale**
>
> Potenziamento delle misure e dei sistemi di `cyber-sicurezza`
>
> **Punteggio:** +1
>
> **Motivo:** sensibilità al tema di cybersecurity

> **10\. Made in Italy, cultura e turismo**
>
> Supporto alla `digitalizzazione dell'intera filiera del settore turistico e della cultura`
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico
---
### IMPEGNO CIVICO
> Nessun argomento tecnologico o di rilievo

---

### LEGA PER SALVINI PREMIER
> **INNOVAZIONE TECNOLOGICA**
>
> Il contributo italiano dovrà essere quello di sottolineare e prevenire i rischi e gl'impatti per `i diritti e le libertà dei cittadini, specialmente per quanto concerne la privacy e la protezione dei dati personali`, da affidarsi ad una governance effettiva, scrupolosa ed indipendente.
>
> **Punteggio:** +1
>
> **Motivo:** sensibilità ai temi della privacy digitale

> **BANDA ULTRA LARGA**
>
> Switch off del Rame entro l'anno 2030
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico


> **VOUCHER CYBERSICUREZZA**
>
> Per alzare davvero i livelli di protezione serve acquistare servizi di nuova generazione, ad alto valore aggiunto. È necessario che la platea di soggetti a cui sarà richiesto un elevato livello di protezione siano sostenuti con un sistema di voucher e crediti d'imposta, `che sostengano gli acquisti di servizi cyber con la finalità di consentire a tutti l'accesso alle tecnologie più moderne ed efficaci`
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **SOVRANITÀ DIGITALE E CYBERSECURITY**
>
> L’Unione Europea ha richiamato più volte il concetto di `autonomia tecnologica`, che si sostanzia con il `detenere tecnologie proprietarie, il cui codice sorgente sia versato su Data Center di aziende europee`.
>
> **Punteggio:** +1
>
> **Motivo:** sovranità digitale, ma non chiaro il supporto a software libero ma almeno GDPR-compliant

> **FORMAZIONE DIGITALE**
>
> `Sostenere le imprese a formare, assumere e a mantenere in Italia i lavoratori del comparto cyber`. 
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **MINISTERO INNOVAZIONE TECNOLOGICA**
>
> `Istituzione di un Ministero dell'innovazione tecnologica` che sappia attuare con efficacia le misure previste dal PNRR: digitalizzazione della pubblica amministrazione e delle imprese, e-government, capitale umano, infrastrutture, connettività, ricerca e sviluppo digitale
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **EDUCAZIONE DIGITALE**
>
> `Promozione dell'educazione digitale` e dell'inclusione tecnologica delle persone che si trovano in situazioni di `divario digitale`, con particolare attenzione ai benefici per le persone con disabilità
>
> **Punteggio:** +1
>
> **Motivo:** contrasto al digital divide

> **IDENTITÀ DIGITALE**
>
> Inserimento in Costituzione della `tutela dell'identità digitale` (assegnata dalla nascita) come il Codice fiscale
>
> **Punteggio:** +1
>
> **Motivo:** tutela identità digitale

> **CONTRASTO DELLE MAFIE DIGITALI**
>
> Il `contrasto alla pirateria audiovisiva` è centrale per le politiche di sostegno del settore. Gli atti di pirateria non rappresentano azioni di singoli "giovani nerd", ma dietro vi è la `criminalità organizzata`
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **SVILUPPARE GLI ISTITUTI PROFESSIONALI COME SCUOLE DI ALTA SPECIALIZZAZIONE**
>
> Serve infatti riallineare l'offerta formativa della filiera tecnica e professionale alla domanda proveniente dai distretti economico-produttivi, alle competenze connesse alla trasformazione dei settori produttivi in linea con il `Piano Nazionale Industria 4.0`, all'`innovazione digitale`, alle trasformazioni nei settori economico-finanziari e nei settori strategici per la competitività
internazionale dell’Italia e alla transizione ecologica.
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **GIOVANI E LAVORO**
>
> Ubicazione al Sud di Centri Nazionali di Eccellenza per la `promozione della tecnologia`, dell'`innovazione`, della `digitalizzazione` e della logistica, in parallelo rispetto allo sviluppo delle ZES
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **PUBBLICA AMMINISTRAZIONE ED EFFICIENZA**
>
> Introdurre l'`uso dell'Intelligenza Artificiale (AI) nella PA, per automatizzare molti processi decisionali basati su semplici procedure di scelta del tipo "Sì/No", In particolare, applicare l'AI nel settore dei contratti e degli appalti pubblici`
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **PUBBLICA AMMINISTRAZIONE ED EFFICIENZA**
>
> Per rendere concretamente possibile il punto precedente, `collegare tutte le banche dati pubbliche già esistenti`
>
> **Punteggio:** -1
>
> **Motivo:** rischio di condivisione dati fra enti pubblici con finalità diverse

> **SICUREZZA URBANA**
>
> Potenziamento dello scambio informativo tra forze di polizia a ordinamento nazionale e polizia locale a ordinamento locale, `potenziamento degli strumenti di videosorveglianza`, potenziamento degli strumenti tecnologici e rafforzamento dell'interconnessione delle sale operative, formazione, aggiornamento comune
>
> **Punteggio:** -1
>
> **Motivo:** aumento videosorveglianza
---
### MOVIMENTO 5 STELLE
> **DALLA PARTE DELLE IMPRESE**
>
> POTENZIAMENTO E STABILIZZAZIONE DECENNALE DI TRANSIZIONE 4.0, estendendo allo stesso piano `Transizione 4.0` la cessione dei crediti d'imposta sul modello del Superbonus
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **PER LA FORMAZIONE**
>
> FAVORIRE L’ACCESSO APERTO AI RISULTATI DELLE RICERCHE.
>
> **Punteggio:** +1
>
> **Motivo:** spinta alla conoscenza aperta

> **STESURA DELLA CARTA DEI DIRITTI DIGITALI**
>
> Riconoscimento dell'`accesso alla rete quale diritto costituzionale` e `copertura nazionale con banda ultralarga`
>
> **Punteggio:** +1
>
> **Motivo:** nuovi diritti digitali

> **BANCA DATI DIGITALE NAZIONALE**
>
> per riconoscere il `diritto all'autodeterminazione informativa, permettendo a tutti di verificare in che modo sono utilizzati i propri dati personali`
>
> **Punteggio:** +1
>
> **Motivo:** nuovi diritti digitali

> **PUBBLICA AMMINISTRAZIONE**
>
> Pubblica amministrazione in `cloud`, `radicale digitalizzazione`, dematerializzazione e `interoperabilità`. Ampliamento "Smarter Italy"
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **DEFINIRE UN PIANO INDUSTRIALE BASATO SULLE TECNOLOGIE STRATEGICHE PER IL FUTURO**
>
> Come manifattura digitale, fintech, valute digitali, intelligenza artificiale e robotica, agrifoodtech, aerospazio, web3, semiconduttori, scienze della vita, creazione di contenuti digitali, metaverso, fino ad arrivare a frontiere come nanotecnologie e quantum computing
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **STEM**
>
> POTENZIARE L'`INSEGNAMENTO DELLE MATERIE STEM SIN DAI PRIMI GRADI SCOLASTICI` E INVESTIRE NELLE COMPETENZE TECNICHE AVANZATE E IMPRENDITORIALI
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico


> **TURISMO**
> 
> ISTITUZIONE DI UNA `PIATTAFORMA PER L’INCONTRO TRA I BISOGNI DEI TURISTI E L’OFFERTA DEL TERRITORIO ITALIANO`, utile a incrementare la capacità di vendita di prodotti e servizi anche delle PMI agricole e artigiane
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico
---
### UNIONE POPOLARE
> **INDUSTRIA**
>
> Investimenti a fondo perduto per accorpamenti di piccole e medie imprese e piani di espansione per mezzo di contratti di programma (utilizzando fondi da `abolizione Transizione 4.0`)
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico anche se potenzialmente negativo
---
### PARTITO DEMOCRATICO
> **Sviluppo sostenibile e transizioni ecologica e digitale**
>
> Vogliamo rafforzare il grande potenziale delle nostre imprese, dei piccoli imprenditori e imprenditrici, delle start-up innovative, del mondo degli artigiani e dei professionisti, attraverso misure di sostegno e di semplificazione, favorendo la `"transizione 4.0"` in uno scenario che coniughi innovazione, concorrenza e sostenibilità
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **Sviluppo sostenibile e transizioni ecologica e digitale**
>
> Vogliamo rimuovere gli ostacoli che frenano le famiglie e le imprese a migrare verso le `reti di connettività a banda ultra-larga`, attuali e future, e al pieno `dispiego delle competenze e dei servizi digitali`, ivi inclusi quelli offerti della Pubblica Amministrazione.
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **Diritti e cittadinanza**
>
> Vogliamo che sia garantita la `trasparenza sui dati e sui contenuti intermediati dalle grandi piattaforme online`. Vogliamo garantire agli utenti dei servizi digitali, nel solco delle normative europee, una `piena cittadinanza digitale`, un `efficace controllo dei propri dati personali`, insieme a un'`efficace regolazione contro gli abusi delle applicazioni dell'intelligenza artificiale`, contro le interferenze delle strategie di disinformazione, `contro forme di tracciamento e di riconoscimento biometrico o l'uso di software di sorveglianza`.
>
> **Punteggio:** +1
>
> **Motivo:** difesa della privacy digitale e contro la sorveglianza


> **Italia 2027: il Paese che vogliamo**
>
> Promuoveremo, inoltre, l'istituzione di un `Fondo nazionale per il diritto alla connessione digitale`, cofinanziato dai risparmi della missione 1.2 del PNRR (circa 1,2 miliardi di euro)
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico


> **Italia 2027: il Paese che vogliamo**
>
> finanziare il `cablaggio verticale degli edifici in fibra ottica` e la `predisposizione di apparati per gli "edifici intelligenti" e l'internet delle cose` anche al fine di coadiuvare il risparmio energetico nelle abitazioni
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico


> **Italia 2027: il Paese che vogliamo**
>
> Dobbiamo affermare, seguendo le linee delle recenti normative europee, il `diritto al pieno controllo economico dei propri dati acquisiti dalle piattaforme digitali` per il loro utilizzo nell’erogazione dei relativi servizi.
>
> **Punteggio:** +1
>
> **Motivo:** difesa della privacy digitale e contro la sorveglianza


> **Italia 2027: il Paese che vogliamo**
>
> Intendiamo inoltre `regolamentare l'utilizzo di big data e dell'intelligenza artificiale`: tale impiego deve trovare un `limite nel rispetto della privacy e della sicurezza delle persone` che porti a `vietare l'uso sistematico di software di sorveglianza, il riconoscimento biometrico dai luoghi pubblici, l’impiego di sistemi di scoring sociale basati sui dati personali`; l'impiego di sistemi di riconoscimento emotivo, l'adozione di `tecnologie digitali dai comprovati effetti discriminatori`. Riteniamo essenziale promuovere un `approccio critico al digitale nel ciclo dell'istruzione, a partire dall'educazione civica digitale fino alla digital literacy e all'educazione sull'uso del dato`, all'impiego di elementi di informatica di base, alla difesa dalla disinformazione
>
> **Punteggio:** +1
>
> **Motivo:** difesa della privacy digitale e contro la sorveglianza


> **Lavoro**
>
> Anticipazione dell'intervento dell'Ue sui lavoratori delle piattaforme online, assicurando `trasparenza sul funzionamento degli algoritmi, che devono essere oggetto di contrattazione collettiva e non possono sostituire l'essere umano nell'assunzione delle decisioni sulle condizioni di lavoro`. In questo quadro occorre porre in capo alle piattaforme l'onere della prova circa l'identificazione del tipo di rapporto di lavoro che si presume subordinato;
>
> **Punteggio:** +1
>
> **Motivo:** lotta contro gli algoritmi decisionali in ambito lavorativo


> **Lavoro**
>
> Promozione dello `smart working`, anche ai fini di favorire le esigenze di conciliazione dei tempi di vita e lavoro, di ridurre le emissioni di agenti inquinanti e di migliorare, nel contempo, la vivibilità dei centri urbani e rivitalizzare i piccoli borghi sempre più spopolati;
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

> **Cultura**
>
> Vogliamo promuovere la `completa digitalizzazione del patrimonio culturale`
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico


> **Giustizia**
>
> Una delle grandi sfide che abbiamo di fronte è quella di `completare la digitalizzazione del servizio giustizia` e adeguare l'organizzazione e l'impostazione dell'intero comparto attraverso l’`organizzazione digitale degli uffici e la creazione di banche dati`.
>
> **Punteggio:** 0
>
> **Motivo:** argomento tecnologico generico

---


### POSSIBILE
> **Saperi e pratiche dell’innovazione**
>
> In merito alle tecnologie digitali, è necessario completare rapidamente la copertura della rete di connessione che colmi il divario digitale fra Nord e Sud e aree periferiche e centrali.
>
> **Punteggio:** +1
>
> **Motivo:** riduzione digital divide


> **Professione docente**
>
> Nel mondo digitale in cui navigano i giovani, le `fake news` sono all'ordine del giorno e selezionare le fonti di informazione, in primis, e comprendere appieno le notizie, poi, non è affatto banale e richiede molto tempo. Per questo motivo, la scuola e gli insegnanti dovrebbero aiutare i ragazzi in questo compito, `dedicando delle ore alla lettura di articoli presenti sul web, ciascuno sulla propria materia di competenza, per seguire anche l’evoluzione della materia stessa e trovare nuovi spunti di interesse per gli studenti`.
>
> **Punteggio:** 0
>
> **Motivo:** spunto interessante per l'alfabetizzazione digitale


> **Lavoro senza barriere**
>
> Pertanto, Possibile propone:
> 
> ● L’adozione di `criteri di accessibilità documentati per i touchpoint digitali`;
> 
> ● `Incentivi e sgravi contributivi per le aziende che assumono persone con disabilità e che adeguano le infrastrutture tecnologiche e i processi per le pari opportunità`;
> 
> ● Sgravi fiscali per le aziende che investono in `formazione e sensibilizzazione in
materia di accessibilità e inclusione sulle infrastrutture digitali`
>
> **Punteggio:** +1
>
> **Motivo:** riduzione digital divide e diritti digitali per disabili


> **Dititale per tutti e tutte**
>
> ● `Limitare l'impatto delle piattaforme digitali proprietarie` attraverso leggi in favore della concorrenza. Le piattaforme proprietarie devono garantire la `massima interoperabilità`, secondo il regolamento europeo del Digital Market Act. Nella PA si dovrebbe favorire l'`adozione del software libero e open source nelle piattaforme digitali per i servizi pubblici` al fine di rendere il sistema PA più indipendente. In particolare, `nel caso dell'uso dei cloud dovrebbe essere garantita la sicurezza e la disponibilità dei dati`;
>
> **Punteggio:** +1
>
> **Motivo:** propensione a software open source e dati aperti


> **Digitale per tutti e tutte**
>
> `Combattere la sorveglianza di massa sulle comunicazioni e la videosorveglianza
di natura biometrica`, che dovrebbe essere messa al bando in tutta l’Unione
europea;
>
> **Punteggio:** +1
>
> **Motivo:** contro la sorveglianza di massa quindi per i diritti di libertà del cittadino


> **Digitale per tutti e tutte**
>
> Promuovere la trasparenza nelle scelte politiche governative attraverso l’impiego
di `basi dati aperte (Open Data)`, il monitoraggio sulla corruzione (attraverso la
tutela sul whistleblowing) e la più diffusa informazione sui mezzi di
comunicazione pubblica;
>
> **Punteggio:** +1
>
> **Motivo:** uso di open data nel pubblico


> **Digitale per tutti e tutte**
>
> `Affermare l'anonimato online come diritto da tutelare contro ogni possibile
autoritarismo` e rivendicare la `difesa della privacy contro la vorace esigenza del
capitalismo della sorveglianza` di profilare tutti i cittadini e tutte le utenze on
line;
>
> **Punteggio:** +1
>
> **Motivo:** contro la sorveglianza di massa e a favore dell'anonimato online


> **Digitale per tutti e tutte**
>
> Circoscrivere il `rapporto tra diritto alla conoscenza, copyright e proprietà
intellettuale` affermando il criterio `che la proprietà intellettuale non sia di
ostacolo al diritto alla conoscenza`, né alle ragioni di salute pubblica e di
sicurezza nazionale;
>
> **Punteggio:** +1
>
> **Motivo:** favorevole al diritto alla conoscenza considerando eventuali ostacoli derivanti dal copyright 


### ITALEXIT
> **Difesa delle libertà costituzionali**
>
> ITALEXIT è `contro ogni forma di` Green Pass e di `identità digitale` e contro l'obbligo vaccinale:
>
> **Punteggio:** 0
>
> **Motivo:** tema tecnologico generico


> **Scuola, cultura, giovani e bambini**
>
> Da qui le proposte per la `creazione di spazi per contrastare l'uso inappropriato dei social network` e le tematiche affini con programmi di prevenzione ai giovani in fascia scolastica.
>
> **Punteggio:** 0
>
> **Motivo:** tema tecnologico generico, interessante


# +EUROPA
> **Diritti e cittadinanza**
>
> di promuovere il ricorso a `strumenti digitali che siano al servizio dei diritti politici dei cittadini`, per dare seguito e completare un pacchetto di riforme che includa la conferma della `possibilità di sottoscrivere digitalmente le proposte di Referendum e di Legge di Iniziativa Popolare`, realizzando la piattaforma online pubblica e gratuita, già prevista dalla legge, dedicata alla raccolta delle sottoscrizioni;
>
> **Punteggio:** +1
>
> **Motivo:** favorevole al diritto di sottoscrivere digitalmente referendum o leggi di iniziativa popolare


> **Pubblica Amministrazione**
>
> ...che le competenze sulla sicurezza digitale diventino diffuse e trasversali a tutti i livelli del personale pubblico e degli utenti dei servizi, `potenziando programmi e strutture in materia di cybersecurity` per le amministrazioni pubbliche.
>
> **Punteggio:** 0
>
> **Motivo:** tema tecnologico generico su cybersecurity


> **Pubblica Amministrazione**
>
> estendere la `formazione digitale ai cittadini`, aggiornando i programmi scolastici prevedendo l’inserimento in tutti i corsi di studi di materie e materiali didattici atti ad aumentare una `maggiore consapevolezza tecnologica` nella popolazione
>
> **Punteggio:** 0
>
> **Motivo:** tema tecnologico generico
